﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence.DTOs;

namespace Assets.Scripts.Persistence
{
    public class FileSystem
    {
        private const string DataPath = @"./data/";
        private const string AttachmentsFileName = @"attachments.xml";
        private const string WeaponsFileName = @"weapons.xml";
        private const string LoadoutFileName = @"loadout.xml";
        private const string MissionParametersFileName = @"missionParameters.xml";

        static string DataFilePath(string fileName) => DataPath + fileName;

        public static void SaveAttachmentData(ModifierSet[] data) => 
            SaveData(data, AttachmentsFileName);

        public static void SaveWeaponData(WeaponStats[] data) =>
            SaveData(data, WeaponsFileName);

        public static void SaveLoadout(LoadoutData data) =>
            SaveData(data, LoadoutFileName);

        public static IEnumerable<ModifierSet> LoadAttachmentData() =>
            LoadData<ModifierSet[]>(AttachmentsFileName);

        public static IEnumerable<WeaponStats> LoadWeaponData() =>
            LoadData<WeaponStats[]>(WeaponsFileName);

        public static LoadoutData LoadLoadout() =>
            LoadData<LoadoutData>(LoadoutFileName);

        public static IEnumerable<MissionParameters> LoadMissionParameters() =>
            LoadData<MissionParameters[]>(MissionParametersFileName);

        static void ValidatePath(string fileName)
        {
            var filePath = DataFilePath(fileName);

            if (!Directory.Exists(DataPath))
                Directory.CreateDirectory(DataPath);

            if (!File.Exists(filePath))
            {
                var file = File.Create(filePath);
                file.Close();
            }
        }

        static void SaveData<T>(T data, string fileName)
        {
            ValidatePath(fileName);
            using (var sw = new StreamWriter(DataFilePath(fileName)))
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(sw, data);
                sw.Flush();
            }
        }

        static T LoadData<T>(string fileName)
        {
            try
            {
                T result;
                ValidatePath(fileName);
                using (var fs = File.Open(DataFilePath(fileName), FileMode.Open))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    result = (T) serializer.Deserialize(fs);
                }

                return result;
            }
            catch
            {
                return default(T);
            }
        }

       
    }
}
