﻿using Assets.Scripts.Persistence.DTOs;

namespace Assets.Scripts.Persistence
{
    public static class CharacterLoadout
    {
        public static string PrimaryWeaponId { get; private set; }
        public static string SecondaryWeaponId { get; private set; }

        public static void SaveLoadout(string primary, string secondary)
        {
            PrimaryWeaponId = primary;
            SecondaryWeaponId = secondary;

            SaveToFile();
        }

        public static void LoadFromFile()
        {
            var data = FileSystem.LoadLoadout();
            PrimaryWeaponId = data.PrimaryWeaponId;
            SecondaryWeaponId = data.SecondaryWeaponId;
        }

        private static void SaveToFile()
        {
            var loadout = FileSystem.LoadLoadout();
            loadout.PrimaryWeaponId = PrimaryWeaponId;
            loadout.SecondaryWeaponId = SecondaryWeaponId;

            FileSystem.SaveLoadout(loadout);
        }
    }
}
