﻿using System;
using Assets.Scripts.City;

namespace Assets.Scripts.Persistence.DTOs
{
    [Serializable]
    public struct MissionParameters
    {
        public MissionType MissionType;
        public string Description;
        public int LevelSize;
        public int MaxDifficulty;

        public int MinCriminals;
        public int MaxCriminals;

        public int MinCivilians;
        public int MaxCivilians;

        public int CriminalsPerDifficulty;
        public int BaseReward;
        public int RewardBonusPerDifficulty;
    }
}
