﻿using System.Collections.Generic;
using Assets.Scripts.InMission.Weapons;

namespace Assets.Scripts.Persistence
{
    public static class InventoryDataPersistance
    {
        private static Dictionary<string, WeaponStats> baseWeaponStats;
        private static Dictionary<string, ModifierSet> attachmentStats;

        public static WeaponStats GetBaseWeaponStats(string weaponId)
        {
            if(baseWeaponStats == null)
                LoadBaseWeaponStats();

            return baseWeaponStats[weaponId];
        }

        public static ModifierSet GetAttachmentStats(string attachmentId)
        {
            if (attachmentStats == null)
                LoadAttachmentStats();

            return attachmentStats[attachmentId];
        }

        public static void LoadBaseWeaponStats()
        {
            baseWeaponStats = new Dictionary<string, WeaponStats>();
            var fileStats = FileSystem.LoadWeaponData();
            foreach (var fws in fileStats)
            {
                baseWeaponStats.Add(fws.WeaponId, fws);
            }
        }

        public static void LoadAttachmentStats()
        {
            attachmentStats = new Dictionary<string, ModifierSet>();
            var fileStats = FileSystem.LoadAttachmentData();
            foreach (var fa in fileStats)
            {
                attachmentStats.Add(fa.AttachmentId, fa);
            }
        }
    }
}
