﻿using UnityEngine;

namespace Assets.Scripts.City
{
    public class Building : MonoBehaviour
    {
        public float Height;

        public Vector3 Top
        {
            get
            {
                var point = transform.position;
                point.y = Height;
                return point;
            }
        }
    }
}