﻿using Assets.Scripts.City.Inventory;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.City.UI
{
    public class CharacterPanel : Panel
    {
        public LoadoutTab LoadoutTab;
        public ArmoryTab ArmoryTab;
        public DossierTab DossierTab;
        public WeaponViewer WeaponViewer;
        public void Initialize()
        {
            LoadoutTab.Initialize();
            ArmoryTab.Initialize(WeaponViewer);
            OnDossierOpen();
        }

        public override void Close()
        {
            LoadoutTab.SaveSettings();
            base.Close();
        }

        public void OnDossierOpen()
        {
            HideTabs();
            DossierTab.Show();
        }

        public void OnArmoryOpen()
        {
            HideTabs();
            ArmoryTab.Show();
        }

        public void OnLoadoutOpen()
        {
            HideTabs();
            LoadoutTab.Show();
        }

        public void GoToShootingRange() => SceneManager.LoadScene("ShootingRange");

        void HideTabs()
        {
            LoadoutTab.Close();
            ArmoryTab.Close();
            DossierTab.Close();
        }
    }
}
