﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.City.UI
{
    public class StatItem : MonoBehaviour
    {
        public Slider StatSlider;
        public Text StatValue;
        public float TextMultiplier;
        public float SliderMultiplier;
        public bool ShowInverted;
        public void SetStat(float value)
        {
            StatSlider.value = ShowInverted ?
                1f / value * SliderMultiplier :
                value * SliderMultiplier;
            
            StatValue.text = ShowInverted ? 
                (1f / (value * TextMultiplier)).ToString("0.0") : 
                (value * TextMultiplier).ToString("0.0");
        }
    }
}
