﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Persistence;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.City.UI
{
    public class LoadoutTab : Panel
    {
        public Dropdown PrimarySelector;
        public Dropdown SecondarySelector;

        private Dictionary<string, string> weaponMap;

        public void Initialize()
        {
            LoadWeaponNames();
            var availableWeaponNames = weaponMap.Values.ToList();
            InitializeSelectors(availableWeaponNames);
        }

        private void InitializeSelectors(List<string> availableWeaponNames)
        {
            PrimarySelector.ClearOptions();
            PrimarySelector.AddOptions(availableWeaponNames);
            PrimarySelector.value = 
                PrimarySelector.options.IndexOf(PrimarySelector.options
                    .First(od => od.text
                        .Equals(weaponMap[CharacterLoadout.PrimaryWeaponId])));

            SecondarySelector.ClearOptions();
            SecondarySelector.AddOptions(availableWeaponNames);
            SecondarySelector.value =
                SecondarySelector.options.IndexOf(SecondarySelector.options
                    .First(od => od.text
                        .Equals(weaponMap[CharacterLoadout.SecondaryWeaponId])));
        }

        public void SaveSettings() => CharacterLoadout.SaveLoadout(
            NameToWeaponId(PrimarySelector.captionText.text),
            NameToWeaponId(SecondarySelector.captionText.text)
            );

        string NameToWeaponId(string weaponName) => 
            weaponMap.First(wm => wm.Value.Equals(weaponName)).Key;

        void LoadWeaponNames()
        {
            weaponMap = new Dictionary<string, string>();
            weaponMap.Add("light-gun", "LDW M2");
            weaponMap.Add("heavy-gun", "D10 Predator");
            weaponMap.Add("auto-shotgun", "CQWP-12 Shotgun");
            weaponMap.Add("smg", "P95 SMG");
            weaponMap.Add("dmr", "L4-DMR");
        }
    }
}
