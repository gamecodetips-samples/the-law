﻿using System;
using System.Collections.Generic;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.City.UI.Armory
{
    public class AttachmentPanel : MonoBehaviour
    {
        public AttachmentSlotButton OpticSlot;
        public AttachmentSlotButton AmmoSlot;
        public AttachmentSlotButton MagSlot;
        public AttachmentSlotButton UnderBarrelSlot;

        public void Initialize(Action<AttachmentType, string> onAttachmentChanged)
        {
            OpticSlot.Initialize(onAttachmentChanged);
            AmmoSlot.Initialize(onAttachmentChanged);
            MagSlot.Initialize(onAttachmentChanged);
            UnderBarrelSlot.Initialize(onAttachmentChanged);
        }

        public void SetAttachmentButtons(IEnumerable<string> attachments)
        {
            OpticSlot.SetTo(attachments);
            AmmoSlot.SetTo(attachments);
            MagSlot.SetTo(attachments);
            UnderBarrelSlot.SetTo(attachments);
        }
    }
}
