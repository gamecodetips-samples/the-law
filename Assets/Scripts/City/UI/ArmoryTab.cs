﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.City.Inventory;
using Assets.Scripts.City.UI.Armory;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence;
using UnityEngine.UI;

namespace Assets.Scripts.City.UI
{
    public class ArmoryTab : Panel
    {
        public Dropdown WeaponSelector;
        public StatViewer StatViewer;
        public AttachmentPanel Attachments;

        private Dictionary<string, string> weaponMap;
        private WeaponViewer weaponViewer;

        private string CurrentWeaponId => NameToWeaponId(WeaponSelector.captionText.text);
        private DisplayWeapon CurrentWeapon => weaponViewer.GetWeapon(CurrentWeaponId);

        public void Initialize(WeaponViewer viewer)
        {
            weaponViewer = viewer;
            weaponViewer.Initialize();

            LoadWeaponNames();
            var availableWeaponNames = weaponMap.Values.ToList();
            Attachments.Initialize(OnAttachmentChanged);
            LoadFromFile();

            InitializeSelector(availableWeaponNames);
            OnWeaponChanged();
        }

        public void OnNextWeapon()
        {
            int nextValue = WeaponSelector.value + 1;
            if (nextValue >= WeaponSelector.options.Count)
                nextValue = 0;

            WeaponSelector.value = nextValue;
        }

        public void OnPreviousWeapon()
        {
            int nextValue = WeaponSelector.value - 1;
            if (nextValue < 0)
                nextValue = WeaponSelector.options.Count - 1;

            WeaponSelector.value = nextValue;
        }

        public void OnWeaponChanged()
        {
            weaponViewer.ShowWeapon(CurrentWeaponId);
            StatViewer.SetStats(CurrentWeapon.CurrentStats);
            Attachments.SetAttachmentButtons(CurrentWeapon.GetAttachmentSerializableData);
        }

        void OnAttachmentChanged(AttachmentType slotType, string attachmentId)
        {
            weaponViewer.SetAttachmentToWeapon(CurrentWeaponId, slotType, attachmentId);
            StatViewer.SetStats(CurrentWeapon.CurrentStats);
            SaveSettings();
        }

        private void InitializeSelector(List<string> availableWeaponNames)
        {
            WeaponSelector.ClearOptions();
            WeaponSelector.AddOptions(availableWeaponNames);
        }

        void LoadFromFile()
        {
            var loadout = FileSystem.LoadLoadout();
            foreach (var w in loadout.AttachmentInfo)
                foreach (var a in w.Attachments)
                    weaponViewer.SetAttachmentToWeapon(w.WeaponId, a);
        }

        void SaveSettings()
        {
            var loadout = FileSystem.LoadLoadout();
            var attachments = CurrentWeapon.GetAttachmentSerializableData;

            loadout.SetAttachments(CurrentWeaponId, attachments);
            FileSystem.SaveLoadout(loadout);
        }

        string NameToWeaponId(string weaponName) =>
            weaponMap.First(wm => wm.Value.Equals(weaponName)).Key;

        void LoadWeaponNames()
        {
            weaponMap = new Dictionary<string, string>();
            weaponMap.Add("light-gun", "LDW M2");
            weaponMap.Add("heavy-gun", "D10 Predator");
            weaponMap.Add("auto-shotgun", "CQWP-12 Shotgun");
            weaponMap.Add("smg", "P95 SMG");
            weaponMap.Add("dmr", "L4-DMR");
        }
    }
}
