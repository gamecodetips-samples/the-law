﻿using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.City.UI
{
    public class StatViewer : MonoBehaviour
    {
        public StatItem Accuracy;
        public StatItem Damage;
        public StatItem RateOfFire;
        public StatItem Handling;

        public void SetStats(WeaponStats stats)
        {
            Accuracy.SetStat(stats.Accuracy);
            Damage.SetStat(stats.Damage.OverallDamage);
            RateOfFire.SetStat(stats.CycleRate);
            Handling.SetStat((stats.ReloadTime + stats.AimSpeed + stats.Recoil.Violence) / 3f);
        }
    }
}
