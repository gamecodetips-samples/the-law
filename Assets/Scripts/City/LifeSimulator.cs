﻿using System.Linq;
using Assets.Scripts.City.Life;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.City
{
    public class LifeSimulator : MonoBehaviour
    {
        public CarMovement CarPrefab;
        public CityNavPointProvider NavPoints;
        public Transform CarContainer;
        
        public int SpawnAmount;

        void Start()
        {
            NavPoints.Initialize();
            SpawnCars();
        }

        private void SpawnCars()
        {
            foreach (var _ in Enumerable.Range(0, SpawnAmount))
            {
                var car = Instantiate(CarPrefab, CarContainer);
                var navPoint = NavPoints.GetRandomNavPoint();
                car.transform.position = navPoint.Position;
                car.Initialize(NavPoints, navPoint);
            }
        }
    }
}
