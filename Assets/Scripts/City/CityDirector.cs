﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Assets.Scripts.City.UI;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Common;
using Assets.Scripts.Persistence;
using Assets.Scripts.Persistence.DTOs;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class CityDirector : MonoBehaviour
    {
        public BuildingProvider BuildingProvider;
        public UIController UI;

        public MissionPin MissionPinPrefab;
        public Transform PinContainer;
        public int MaxMissionCount;
        public int CaseRequirement;

        void Start()
        {
            //DataFilesCreator.CreateWeaponData();
            //DataFilesCreator.CreateAttachmentData();
            CharacterLoadout.LoadFromFile();
            var parameters = LoadMissionParameterData();

            UI.Initialize(CaseRequirement);
            BuildingProvider.Initialize();
            GenerateMissions(
                Random.Range(1, MaxMissionCount+1),
                parameters.Where(mp => mp.MissionType != MissionType.Boss));
            if (ProgressPersistence.CurrentProgress >= CaseRequirement)
                GenerateBossMission(parameters.Where(mp => mp.MissionType == MissionType.Boss));

        }

        private void GenerateBossMission(IEnumerable<MissionParameters> parameters) => 
            CreateMissionPin(parameters.PickOne());

        private IEnumerable<MissionParameters> LoadMissionParameterData() => 
            FileSystem.LoadMissionParameters();

        private void GenerateMissions(int missionCount, IEnumerable<MissionParameters> parameters)
        {
            foreach (var _ in Enumerable.Range(0, missionCount))
                CreateMissionPin(parameters.PickOne());
        }

        private void CreateMissionPin(MissionParameters parameters)
        {
            var mission = Mission.GenerateNew(parameters);
            var pin = Instantiate(MissionPinPrefab, BuildingProvider.GetRandomBuilding().Top, Quaternion.identity);
            pin.transform.SetParent(PinContainer);
            pin.Initialize(mission, UI.ShowMissionPanel);
        }
    }
}
