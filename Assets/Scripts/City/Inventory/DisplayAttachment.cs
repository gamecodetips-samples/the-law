﻿using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence;
using UnityEngine;

namespace Assets.Scripts.City.Inventory
{
    public class DisplayAttachment : MonoBehaviour
    {
        public string AttachmentId;
        public AttachmentType AttachmentType;

        [HideInInspector]
        public ModifierSet ModifierSet;

        public void Initialize() =>
            ModifierSet = InventoryDataPersistance.GetAttachmentStats(AttachmentId);
    }
}
