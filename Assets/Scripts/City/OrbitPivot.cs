﻿using UnityEngine;

namespace Assets.Scripts.City
{
    public class OrbitPivot : MonoBehaviour
    {
        public float RotationSpeed;

        void Update()
        {
            var horizontal = Input.GetAxis("Horizontal");
            horizontal += Input.GetButton("Aim") ? Input.GetAxis("Mouse X") : 0;

            Orbit(horizontal);
                
        }

        void Orbit(float factor) => transform.Rotate(Vector3.up * factor * RotationSpeed * Time.deltaTime);
    }
}
