﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class FaceCamera : MonoBehaviour
    {
        public bool LimitToYaw;

        void Update()
        {
            var rotation = Camera.main.transform.eulerAngles;
            if (LimitToYaw)
                rotation.x = 0;

            transform.eulerAngles = rotation;
        }
    }
}
