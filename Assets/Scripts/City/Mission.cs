﻿using Assets.Scripts.Persistence.DTOs;
using UnityEngine;

namespace Assets.Scripts.City
{
    public class Mission
    {
        public string Description { get; private set; }
        public MissionType MissionType { get; private set; }
        public int Difficulty { get; private set; }
        public int CriminalCount { get; private set; }
        public int CivilianCount { get; private set; }
        public int BaseReward { get; private set; }
        public int LevelSize { get; private set; }

        public int RewardPerCivilian { get; private set; }

        public static Mission GenerateNew(MissionParameters parameters)
        {
            var difficulty = Random.Range(1, parameters.MaxDifficulty + 1);
            var criminalCount = Random.Range(parameters.MinCriminals, parameters.MaxCriminals + 1) +
                                difficulty * parameters.CriminalsPerDifficulty;
            var civilianCount = Random.Range(parameters.MinCivilians, parameters.MaxCivilians + 1);
            var reward = parameters.BaseReward + (parameters.RewardBonusPerDifficulty * difficulty);

            return new Mission
            {
                Description = parameters.Description,
                MissionType = parameters.MissionType,
                Difficulty = difficulty,
                CriminalCount = criminalCount,
                CivilianCount = civilianCount,
                BaseReward = reward,
                LevelSize = parameters.LevelSize,
                RewardPerCivilian = 2
            };
        }
    }
    
    public enum MissionType
    {
        Raid,
        Bomb,
        Boss,
        Hostages
    }
}