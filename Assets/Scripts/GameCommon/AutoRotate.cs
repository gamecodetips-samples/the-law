﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.GameCommon
{
    public class AutoRotate : MonoBehaviour
    {
        public Vector3 RotationAxis;
        public float Speed;

        void Update() => 
            transform.Rotate(RotationAxis.normalized * Speed);
    }
}
