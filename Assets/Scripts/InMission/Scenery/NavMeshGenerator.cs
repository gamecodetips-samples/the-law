﻿using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.InMission.Scenery
{
    public class NavMeshGenerator : MonoBehaviour
    {
        public NavMeshSurface builder;

        void Start()
        {
            builder.BuildNavMesh();
        }
    }
}
