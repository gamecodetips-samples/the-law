﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.InMission.Common;
using UnityEngine;

namespace Assets.Scripts.InMission.Character
{
    public class CharacterInteraction : MonoBehaviour
    {
        public float InteractionReach;
        public LayerMask InteractionLayer;
        
        public void Interact()
        {
            var ray = new Ray(transform.position, transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, InteractionReach, InteractionLayer))
            {
                var interactable = hit.collider.GetComponent<Interactable>();
                interactable.OnInteract(this.transform);
            }
        }
    }
}
