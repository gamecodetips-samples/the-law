﻿using UnityEngine;

namespace Assets.Scripts.InMission.Character
{
    public class CharacterMovement : MonoBehaviour
    {
        public float MoveSpeedFactor;
        public float CrouchingSpeedFactor;

        private bool isCrouching;
        private Animator animator;

        public void Initialize(Animator aimator) => this.animator = aimator;

        public void MoveTo(Vector3 direction)
        {
            var speedFactor = isCrouching ? CrouchingSpeedFactor : MoveSpeedFactor;
            transform.Translate(direction * speedFactor * Time.deltaTime);
        }

        public void ToggleCrouch()
        {
            isCrouching = !isCrouching;
            animator.SetBool("IsCrouching", isCrouching);
        }
    }
}