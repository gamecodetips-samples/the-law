﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.Persistence;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class Weapon : MonoBehaviour
    {
        public string WeaponId;
        public WeaponAttachmentData AttachmentData;
        public WeaponView View;

        public Vector3 FirePosition => referencePoint.position;
        private bool isCycling;
        private bool isReloading;
        protected int currentAmmo;
        private bool isAiming;
        protected Transform referencePoint;
        protected WeaponStats BaseStats;

        [HideInInspector]
        public WeaponStats CurrentStats;
        public bool IsAuto => BaseStats.IsAutomatic;
        public Vector3 AimPoint => GetComponentInChildren<AimpointPerspective>().LocalOffset(this.transform);

        private float currentAccuracy => isAiming ? CurrentStats.Accuracy + CurrentStats.AimBonus : CurrentStats.Accuracy;

        protected float Inaccuracy => 1f / currentAccuracy;

        public bool IsIdle => !isCycling & !isReloading;
        public bool IsEmpty => currentAmmo <= 0;

        public void Initialize(Transform referencePoint, ModifierSet actorModifierSet)
        {
            this.referencePoint = referencePoint;
            LoadWeaponStats(WeaponId);

            var modifierSets = GetComponentsInChildren<Attachment>()
                .Select(a => a.ModifierSet)
                .Plus(actorModifierSet);

            CalculateCurrentStats(modifierSets);

            currentAmmo = CurrentStats.MagazineSize;
            View.Initialize(CurrentStats);
        }

        private void LoadWeaponStats(string weaponId) =>
            BaseStats = InventoryDataPersistance.GetBaseWeaponStats(weaponId);

        public virtual void Fire() { }

        public void Reload()
        {
            if (IsIdle)
            {
                View.PlayReload();
                StartCoroutine(WaitForReload());
            }
        }

        private IEnumerator WaitForReload()
        {
            isReloading = true;
            yield return new WaitForSeconds(CurrentStats.ReloadTime);
            isReloading = false;
            currentAmmo = CurrentStats.MagazineSize;
        }

        protected void CycleRound() => StartCoroutine(WaitForCycle());

        private IEnumerator WaitForCycle()
        {
            isCycling = true;
            yield return new WaitForSeconds(CurrentStats.CycleRate);
            isCycling = false;
        }

        public void SetAim(bool isAiming) => this.isAiming = isAiming;

        public void AddAttachment(Attachment attachment)
        {
            var parent = AttachmentData.AttachmentContainer;

            if (attachment.AttachmentType == AttachmentType.Optic)
                parent = AttachmentData.OpticSpot;
            else if (attachment.AttachmentType == AttachmentType.UnderBarrel)
                parent = AttachmentData.UnderBarrelSpot;

            attachment.transform.SetParent(parent);
            attachment.transform.localPosition = Vector3.zero;
            attachment.transform.localRotation = Quaternion.identity;
        }

        protected virtual void CalculateCurrentStats(IEnumerable<ModifierSet> modifierSets)
        {
            CurrentStats = BaseStats;
            foreach (var ms in modifierSets)
            {
                CurrentStats.ReloadTime += ms.ReloadTime;
                CurrentStats.AimBonus += ms.AimBonus;
                CurrentStats.AimSpeed += ms.AimSpeed;
                CurrentStats.AimZoom += ms.AimZoom;
                CurrentStats.TunnelVisionFactor += ms.TunnelVisionFactor;
                CurrentStats.Accuracy += ms.Accuracy;
                CurrentStats.Recoil.Violence += ms.RecoilViolence;
                if (ms.MagazineMultiplier > 0)
                    CurrentStats.MagazineSize = (int) (BaseStats.MagazineSize * ms.MagazineMultiplier);
                CurrentStats.Damage = BaseStats.Damage.ApplyModifier(ms.Damage);
            }
        }

        public void PlaceAt(Transform placement)
        {
            transform.SetParent(placement);
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}
