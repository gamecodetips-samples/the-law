﻿using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Props;
using Assets.Scripts.InMission.Scenery;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class HitscanWeapon : Weapon
    {
        public LayerMask HittableLayerMask;
     
        public override void Fire()
        {
            var deviationOffset = Random.insideUnitSphere * Inaccuracy;
            var totalDeviation = (referencePoint.forward + deviationOffset).normalized; 
            
            CastFireRay(FirePosition, totalDeviation);
            View.ShowFire(totalDeviation);
            currentAmmo--;

            CycleRound();
        }

        protected void CastFireRay(Vector3 firePosition, Vector3 deviation)
        {
            var ray = new Ray(FirePosition, deviation);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100f, HittableLayerMask))
            {
                CheckSurfaceHit(hit);
                CheckDamageableHit(hit);
                CheckPush(hit);
            }
        }

        private void CheckPush(RaycastHit hit)
        {
            var pushable = hit.collider.GetComponent<Push>();
            pushable?.PushTo(transform.forward, CurrentStats.PushForce);
        }
        
        private void CheckDamageableHit(RaycastHit hit)
        {
            var armorPiece = hit.collider.GetComponent<Damageable>();
            armorPiece?.GetHit(CurrentStats.Damage);
        }

        private void CheckSurfaceHit(RaycastHit hit)
        {
            var surface = hit.collider.GetComponent<HitSurface>();
            surface?.HitHere(hit.point, hit.normal);
        }
    }
}
