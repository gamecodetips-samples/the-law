﻿using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class AimpointPerspective : MonoBehaviour
    {
        public Vector3 LocalOffset(Transform referenceTransform) => referenceTransform.InverseTransformPoint(transform.position);
    }
}
