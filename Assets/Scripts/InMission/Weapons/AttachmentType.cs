﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.InMission.Weapons
{
    public enum AttachmentType
    {
        Optic,
        UnderBarrel,
        Magazine,
        Receiver
    }
}
