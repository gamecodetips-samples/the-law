﻿using System.Collections;
using Assets.Scripts.InMission.Directors;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.InMission.Weapons.ShoulderCannon
{
    public class StunGrenade : MonoBehaviour
    {
        public Rigidbody Body;
        public AudioSource ExplosionSfx;
        public ParticleSystem ExplosionVfx;

        private float stunDuration;
        private float stunRange;

        public void Initialize(float fuseLength, float stunDuration, float stunRange, float launchStrength)
        {
            this.stunDuration = stunDuration;
            this.stunRange = stunRange;

            Body.AddForce(transform.forward * launchStrength);
            StartCoroutine(WaitAndDetonate(fuseLength));

        }

        IEnumerator WaitAndDetonate(float fuseLength)
        {
            yield return new WaitForSeconds(fuseLength);
            Detonate();
        }

        private void Detonate()
        {
            ExplosionVfx.Play();
            ExplosionSfx.Play();
            var criminalDirector = FindObjectOfType<CriminalDirector>();
            if (criminalDirector)
                StunActors(criminalDirector);
        }

        private void StunActors(CriminalDirector criminalDirector)
        {
            var actorsInRange = criminalDirector.GetActorsInRange(transform.position, stunRange);
            foreach (var a in actorsInRange)
                a.Stun(stunDuration);
        }
    }
}
