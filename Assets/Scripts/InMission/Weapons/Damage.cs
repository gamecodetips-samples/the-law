﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.InMission.Weapons
{
    [Serializable]
    public struct Damage
    {
        public int FleshDamage;
        public int ArmorDamage;
        public int ElectricDamage;
        public float OverallDamage => (FleshDamage + ArmorDamage + ElectricDamage) / 3f;
        
        public Damage(int basedamage)
        {
            FleshDamage = basedamage;
            ArmorDamage = (int)(basedamage * 0.8f);
            ElectricDamage = (int)(basedamage * 0.6f);
        }

        public Damage ApplyModifier(DamageModifier modifier)
        {
            if (modifier.FleshDamageMultiplier > 0)
            {
                return new Damage
                {
                    FleshDamage = (int) (FleshDamage * modifier.FleshDamageMultiplier),
                    ArmorDamage = (int) (ArmorDamage * modifier.ArmorDamageMultiplier),
                    ElectricDamage = (int) (ElectricDamage * modifier.ElectricDamageMultiplier),
                };
            }

            return this;
        }
    }
}
