﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.InMission.Weapons
{
    public class Scattergun : HitscanWeapon
    {
        public int FragmentCount;

        public override void Fire()
        {
            foreach (var _ in Enumerable.Range(0, FragmentCount))
            {
                var deviationOffset = Random.insideUnitSphere * Inaccuracy;
                var totalDeviation = (referencePoint.forward + deviationOffset).normalized;

                CastFireRay(FirePosition, totalDeviation);
            }

            View.ShowFire();
            currentAmmo--;
            CycleRound();
        }

        protected override void CalculateCurrentStats(IEnumerable<ModifierSet> modifierSets)
        {
            base.CalculateCurrentStats(modifierSets);
            CurrentStats.Damage.FleshDamage /= FragmentCount;
            CurrentStats.Damage.ArmorDamage /= FragmentCount;
            CurrentStats.Damage.ElectricDamage /= FragmentCount;
        }
    }
}
