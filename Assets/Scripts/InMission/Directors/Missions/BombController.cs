﻿using System;
using Assets.Scripts.City;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Player;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.Persistence;

namespace Assets.Scripts.InMission.Directors.Missions
{
    public class BombController : MissionController
    {
        Action onLevelComplete;
        Mission mission;

        public BombController(
            Mission mission, 
            CivilianDirector civilianDirector, 
            CriminalDirector criminalDirector,
            BombDirector bombDirector,
            HUD hud,
            SpawnPointProvider spawnPoints,
            NavPointProvider navPoints,
            CoverSpotProvider coverSpots, 
            PlayerInitializer playerCharacter,
            Action onLevelComplete)
        {
            this.mission = mission;
            this.onLevelComplete = onLevelComplete;

            var civCount = mission.CivilianCount;
            var crimCount = mission.CriminalCount;

            civilianDirector.Initialize(spawnPoints, civCount, navPoints);
            criminalDirector.Initialize(spawnPoints, crimCount, coverSpots, playerCharacter);
            bombDirector.Initialize(spawnPoints, 1, 300, OnLevelFailed, OnLevelSuccess);
        }
        
        public void OnLevelFailed()
        {
            ProgressPersistence.ModifyProgress(-mission.BaseReward);
            onLevelComplete();
        }

        public void OnLevelSuccess()
        {
            var reward = mission.BaseReward;
            ProgressPersistence.ModifyProgress(reward);
            onLevelComplete();
        }
    }
}
