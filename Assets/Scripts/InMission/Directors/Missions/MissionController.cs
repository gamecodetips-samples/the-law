﻿namespace Assets.Scripts.InMission.Directors.Missions
{
    public interface MissionController
    {
        void OnLevelFailed();
        void OnLevelSuccess();
    }
}
