﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Props;
using UnityEngine;

namespace Assets.Scripts.InMission.Directors
{
    public class BombDirector : MonoBehaviour
    {
        public Bomb BombPrefab;

        IList<Bomb> bombs = new List<Bomb>();

        private Action onAllBombsDefused = () => { };
        private Action onBombsExplode = () => { };
        
        public void Initialize(SpawnPointProvider spawnPoints, int amount, float fuseLength, 
            Action onBombsExplode, Action onAllBombsDefused)
        {
            this.onBombsExplode = onBombsExplode;
            this.onAllBombsDefused = onAllBombsDefused;

            Debug.Log("initializing bomb mission");
            foreach (var _ in Enumerable.Range(0, amount))
            {
                var candidate = spawnPoints.GetBombSpawnPoint();
                if (candidate.Exists)
                {
                    var bomb = Instantiate(BombPrefab, candidate.Value.Position, Quaternion.identity);
                    bombs.Add(bomb);
                    bomb.transform.SetParent(transform);
                    bomb.Initialize(fuseLength, OnBombDefused);
                    Debug.Log("bomb created");
                }
            }

            StartCoroutine(BombTimers(fuseLength));
        }

        IEnumerator BombTimers(float fuseLength)
        {
            yield return new WaitForSeconds(fuseLength);

            foreach (var b in bombs)
                b.Detonate();

            onBombsExplode();
        }

        void OnBombDefused(Bomb bomb)
        {
            bombs.Remove(bomb);
            if (!bombs.Any())
            {
                onAllBombsDefused();
                StopAllCoroutines();
            }
        }
    }
}
