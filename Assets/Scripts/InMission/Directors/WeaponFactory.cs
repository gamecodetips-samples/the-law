﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Directors
{
    public class WeaponFactory : MonoBehaviour
    {
        public Weapon[] WeaponPrefabs;
        public Attachment[] AttachmentPrefabs;

        public Weapon Create(string weaponId) => 
            Instantiate(WeaponPrefabs.First(w => w.WeaponId.MatchId(weaponId)));

        public Weapon Create(string weaponId, IEnumerable<string> attachmentIds)
        {
            var weapon = Create(weaponId);
            foreach (var aid in attachmentIds)
            {
                var attachment = CreateAttachment(aid);
                weapon.AddAttachment(attachment);
            }

            return weapon;
        }

        Attachment CreateAttachment(string attachmentId)
        {
            var attachmentPrefabs = AttachmentPrefabs.First(a => a.AttachmentId.MatchId(attachmentId));
            var attachment = Instantiate(attachmentPrefabs);
            attachment.Initialize();

            return attachment;
        }
    }
}
