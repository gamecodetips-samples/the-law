﻿using System;
using System.Collections;
using Assets.Scripts.City;
using Assets.Scripts.InMission.Directors.Missions;
using Assets.Scripts.InMission.Navigation;
using Assets.Scripts.InMission.Player;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.Persistence;
using Assets.Scripts.Persistence.DTOs;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.InMission.Directors
{
    public class LevelDirector : MonoBehaviour
    {
        [Header("References")]
        public global::LevelGenerator.Scripts.LevelGenerator Generator;
        public PlayerInitializer Player;
        public CivilianDirector CivilianDirector;
        public CriminalDirector CriminalDirector;
        public BombDirector BombDirector;
        public CoverSpotProvider CoverSpots;
        public NavPointProvider NavPoints;
        public SpawnPointProvider SpawnPoints;
        public HUD HUD;

        private Mission mission;
        private MissionController missionController;

        void Start()
        {
            mission = LoadMission();
            Generator.Generate(mission.LevelSize);
            StartCoroutine(WaitAndDo(1f, InitializeAll));
        }

        private Mission LoadMission()
        {
            var current = ProgressPersistence.CurrentMission;
            if (current == null)
            {
                current = Mission.GenerateNew(new MissionParameters
                {
                    LevelSize = 20
                });
            }

            return current;
        }

        private IEnumerator WaitAndDo(float time, Action doOnWait)
        {
            yield return new WaitForSeconds(time);
            doOnWait();
        }

        void InitializeAll()
        {
            SpawnPoints.Initialize();
            NavPoints.Initialize();
            CoverSpots.Initialize();

            InitializeMission(mission);

            Player.Ready(SpawnPoints.PlayerSpwawnPoint.transform, missionController.OnLevelFailed);
        }

        private void InitializeMission(Mission mission)
        {
            switch (mission.MissionType)
            {
                case MissionType.Hostages:
                    missionController = new HostagesController(mission, CivilianDirector, CriminalDirector, HUD,
                        SpawnPoints, NavPoints, CoverSpots, Player, LevelFinished);
                    break;
                case MissionType.Bomb:
                    missionController = new BombController(mission, CivilianDirector, CriminalDirector, BombDirector, HUD,
                        SpawnPoints, NavPoints, CoverSpots, Player, LevelFinished);
                    break;
                case MissionType.Raid:
                    missionController = 
                        new RaidController(mission, CivilianDirector, CriminalDirector, HUD, 
                            SpawnPoints, NavPoints, CoverSpots, Player, LevelFinished);
                    break;
                case MissionType.Boss:
                    missionController =
                        new BossFightController(mission, CivilianDirector, CriminalDirector, HUD,
                            SpawnPoints, NavPoints, CoverSpots, Player, LevelFinished);
                    break;
            }
        }

        void LevelFinished() => StartCoroutine(WaitAndGoBackToCity());

        IEnumerator WaitAndGoBackToCity()
        {
            HUD.ShowFadeOut();
            yield return new WaitForSeconds(4f);
            SceneManager.LoadScene("Cityscape");
        }
    }
}
