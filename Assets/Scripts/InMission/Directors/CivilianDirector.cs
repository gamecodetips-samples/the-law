﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.InMission.Actor;
using Assets.Scripts.InMission.Navigation;
using UnityEngine;

namespace Assets.Scripts.InMission.Directors
{
    public class CivilianDirector : MonoBehaviour
    {
        public CivilianController CivilianPrefab;
        public HostageController HostagePrefab;

        IList<CivilianController> civilians = new List<CivilianController>();
        IList<HostageController> hostages = new List<HostageController>();

        private Action<int> onCivilianDeath = (a) => { };
        private Action onAllHostagesEvacuated = () => { };

        public int Remaining => civilians.Count(c => c.IsAlive);

        public void Initialize(SpawnPointProvider spawnPoints, int amount, NavPointProvider navigationProvider,
            Action<int> onCivilianDeath)
        {
            this.onCivilianDeath = onCivilianDeath;

            Initialize(spawnPoints, amount, navigationProvider);
        }

        public void Initialize(SpawnPointProvider spawnPoints, int amount, NavPointProvider navigationProvider)
        {
            foreach (var _ in Enumerable.Range(0, amount))
            {
                var candidate = spawnPoints.GetSpawnPoint();
                if (candidate.Exists)
                {
                    var civ = Instantiate(CivilianPrefab);
                    civilians.Add(civ);
                    civ.transform.SetParent(transform);
                    civ.Initialize(navigationProvider, candidate.Value.Position, UpdateDeadCivilians);
                }
            }

            UpdateDeadCivilians();
        }

        void UpdateDeadCivilians()
        {
            var deadCivilans = civilians.Count(c => !c.IsAlive);
            onCivilianDeath(deadCivilans);
        }

        public void InitializeHostages(SpawnPointProvider spawnPoints, int amount, NavPointProvider navigationProvider, Action onHostageKilled, Action onAllHostagesEvacuated)
        {
            this.onAllHostagesEvacuated = onAllHostagesEvacuated;

            foreach (var _ in Enumerable.Range(0, amount))
            {
                var candidate = spawnPoints.GetSpawnPoint();
                if (candidate.Exists)
                {
                    var hostage = Instantiate(HostagePrefab);
                    hostages.Add(hostage);
                    hostage.transform.SetParent(transform);
                    hostage.Initialize(navigationProvider, candidate.Value.Position, onHostageKilled, OnHostageExited);
                }
            }
        }

        private void OnHostageExited(HostageController hostage)
        {
            hostages.Remove(hostage);
            if(!hostages.Any())
                onAllHostagesEvacuated();
        }
    }
}
