﻿using Assets.Scripts.InMission.Character;
using Assets.Scripts.InMission.Weapons.ShoulderCannon;
using UnityEngine;

namespace Assets.Scripts.InMission.Player
{
    public class PlayerInput : MonoBehaviour
    {
        private CharacterMovement movement;
        private WeaponController weapon;
        private ShoulderCannon shoulderCannon;
        private CharacterLook look;
        private CharacterInteraction interaction;

        public void Initialize(PlayerCharacter character)
        {
            enabled = true;
            Cursor.lockState = CursorLockMode.Locked;

            movement = character.Movement;
            look = character.Look;
            weapon = character.Weapon;
            shoulderCannon = character.ShoulderCannon;
            interaction = character.Interaction;
        }

        void Update()
        {
            UpdateKeyboard();
            UpdateMouse();
        }

        void UpdateKeyboard()
        {
            var moveVector = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
            movement.MoveTo(moveVector);

            if(Input.GetButtonDown("Crouch"))
                movement.ToggleCrouch();

            if (Input.GetButtonDown("Interact"))
                interaction.Interact();

            if (Input.GetButtonDown("FireCannon"))
                shoulderCannon.Fire();
        }
        void UpdateMouse()
        {
            var lookVector = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            look.LookTo(lookVector);

            if (Input.GetButton("Fire"))
                weapon.FireAuto();

            if (Input.GetButtonDown("Fire"))
                weapon.FireSemi();

            if (Input.GetButtonDown("Aim"))
                weapon.SwitchAim();

            if (Input.GetButtonDown("Reload"))
                weapon.Reload();

            if(Input.GetAxis("SwapWeapon") > 0.01f)
                weapon.SwapToNext();
        }
    }
}
