﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.GameCommon;
using Assets.Scripts.InMission.Character;
using Assets.Scripts.InMission.Directors;
using Assets.Scripts.InMission.UI;
using Assets.Scripts.InMission.Weapons;
using Assets.Scripts.Persistence;
using UnityEngine;

namespace Assets.Scripts.InMission.Player
{
    public class PlayerInitializer : MonoBehaviour
    {
        public HUD HUD;
        public PlayerInput PlayerInput;
        public PlayerCharacter Character;
        public WeaponFactory WeaponFactory;

        private Action onDeath;
            
        public void Ready(Transform startSpot, Action onDeath)
        {
            this.onDeath = onDeath;
            Character.Initialize(HUD, startSpot, StartingWeapons(), OnCharacterDeath);
            PlayerInput.Initialize(Character);
        }

        private IEnumerable<Weapon> StartingWeapons()
        {
            var loadout = FileSystem.LoadLoadout();
            var primaryWeaponAttachments = loadout.AttachmentInfo
                .First(ai => ai.WeaponId.MatchId(loadout.PrimaryWeaponId)).Attachments;
            var secondaryWeaponAttachments = loadout.AttachmentInfo
                .First(ai => ai.WeaponId.MatchId(loadout.SecondaryWeaponId)).Attachments;

            var weapons = new List<Weapon>();
            weapons.Add(WeaponFactory.Create(loadout.PrimaryWeaponId, primaryWeaponAttachments));
            weapons.Add(WeaponFactory.Create(loadout.SecondaryWeaponId, secondaryWeaponAttachments));
            return weapons;
        }

        private void OnCharacterDeath()
        {
            PlayerInput.enabled = false;
            Character.Die();
            onDeath();
        }
    }
}
