﻿using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Props
{
    public class ArmorPiece : MonoBehaviour, Damageable
    {
        public int ArmorPoints;

        void Destroy()
        {
            gameObject.SetActive(false);
        }

        public void GetHit(Damage damage)
        {
            ArmorPoints -= damage.ArmorDamage;
            if(ArmorPoints <= 0)
                Destroy();
        }
    }
}
