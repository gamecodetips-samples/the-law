﻿using Assets.Scripts.InMission.Common;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Props
{
    public class EnergyShield : MonoBehaviour, Damageable
    {
        public int HitPoints;

        void Destroy()
        {
            gameObject.SetActive(false);
        }

        public void GetHit(Damage damage)
        {
            HitPoints -= damage.ElectricDamage;
            if(HitPoints <= 0)
                Destroy();
        }
    }
}
