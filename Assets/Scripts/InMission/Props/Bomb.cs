﻿using System;
using Assets.Scripts.InMission.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.InMission.Props
{
    public class Bomb : MonoBehaviour
    {
        public Interactable Interactable;
        public Text DisplayScreen;
        public AudioSource ExplosionSfx;

        private bool isActive;
        private float fuse;
        private Action<Bomb> onDefused;

        public void Initialize(float fuseLength, Action<Bomb> onDefused)
        {
            this.onDefused = onDefused;
            isActive = true;
            fuse = fuseLength;
            Interactable.Initialize(Defuse);
        }

        public void Defuse(Transform _)
        {
            isActive = false;
            onDefused(this);
        }

        void Update()
        {
            if(isActive)
            {
                fuse -= Time.deltaTime;
                DisplayScreen.text = fuse.ToString("000");
            }
        }

        public void Detonate() => ExplosionSfx.Play();
    }
}
