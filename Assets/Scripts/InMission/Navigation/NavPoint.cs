﻿using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class NavPoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;
    }
}
