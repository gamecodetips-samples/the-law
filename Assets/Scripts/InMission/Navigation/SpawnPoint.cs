﻿using UnityEngine;

namespace Assets.Scripts.InMission.Navigation
{
    public class SpawnPoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;
    }
}
