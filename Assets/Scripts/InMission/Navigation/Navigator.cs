﻿using System;
using Assets.Scripts.InMission.Actor;
using UnityEngine;
using UnityEngine.AI;

namespace Assets.Scripts.InMission.Navigation
{
    public class Navigator : MonoBehaviour
    {
        public NavMeshAgent Agent;
        public float ProximityThreshold;

        private Vector3 currentTarget;
        private bool hasTarget;
        private ActorAnimator animator;
        private Action onDestinationReached;

        private bool CloseToTarget =>
            Vector3.Distance(transform.position, currentTarget) < ProximityThreshold;

        public void Initialize(ActorAnimator animator, Vector3 spawnLocation)
        {
            Agent.Warp(spawnLocation);
            Agent.enabled = true;
            this.animator = animator;
        }

        public void MoveTo(Vector3 target)
        {
            currentTarget = target;
            Agent.SetDestination(currentTarget);
            hasTarget = true;
            Agent.isStopped = false;
            animator.SetMoveSpeed(Agent.speed);

            onDestinationReached = () => { };
        }

        public void MoveTo(Vector3 target, Action doOnComplete)
        {
            MoveTo(target);
            onDestinationReached = doOnComplete;
        }

        void Update()
        {
            if (hasTarget & CloseToTarget)
            {
                Agent.isStopped = true;
                hasTarget = false;
                animator.StopMoving();

                onDestinationReached();
            }
        }

        public void Shutdown()
        {
            Agent.isStopped = true;
            hasTarget = false;
            Agent.enabled = false;
            this.enabled = false;
        }

        public void Stop() => MoveTo(transform.position);
    }
}
