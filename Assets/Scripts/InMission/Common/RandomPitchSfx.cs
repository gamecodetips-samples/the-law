﻿using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class RandomPitchSfx : MonoBehaviour
    {
        public float MinPitch;
        public float MaxPitch;
        public bool PlayOnAwake;

        private AudioSource source;

        void Start()
        {
            if(PlayOnAwake)
                Play();
        }
        
        private AudioSource Source
        {
            get
            {
                if (!source)
                    source = GetComponent<AudioSource>();

                return source;
            }
        }

        public void Play()
        {
            Source.pitch = Random.Range(MinPitch, MaxPitch);
            Source.Play();
        }
    }
}
