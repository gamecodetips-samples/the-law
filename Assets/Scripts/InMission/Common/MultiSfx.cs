﻿using Assets.Scripts.GameCommon;
using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class MultiSfx : MonoBehaviour
    {
        public AudioClip[] Clips;

        private AudioSource source;
        private AudioSource Source
        {
            get
            {
                if (!source)
                    source = GetComponent<AudioSource>();

                return source;
            }
        }

        public void Play()
        {
            Source.clip = Clips.PickOne();
            Source.Play();
        }
    }
}
