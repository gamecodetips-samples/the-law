﻿namespace Assets.Scripts.InMission.Common
{
    public struct Maybe<T>
    {
        public T Value { get; private set; }
        public bool Exists => Value != null;

        public Maybe(T value) => Value = value;

        public static Maybe<T> Empty() => new Maybe<T> {Value = default(T)};
    }
}