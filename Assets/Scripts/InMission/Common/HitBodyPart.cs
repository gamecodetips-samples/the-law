﻿using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Common
{
    public class HitBodyPart : MonoBehaviour, Damageable
    {
        public float DamageMultiplier;

        private Health health;

        public void Initialize(Health health) => this.health = health;

        public void GetHit(Damage damage) => 
            health.ReceiveDamage(Mathf.CeilToInt(damage.FleshDamage * DamageMultiplier));
    }
}
