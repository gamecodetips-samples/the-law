﻿using System.Collections.Generic;
using Assets.Scripts.InMission.Common;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class Ragdoll : MonoBehaviour
    {
        IEnumerable<Rigidbody> rigidBodies;

        public void Initialize()
        {
            rigidBodies = GetComponentsInChildren<Rigidbody>();

            InitializePushComponents();
            TurnOffRagdoll();
        }

        private void InitializePushComponents()
        {
            foreach (var p in GetComponentsInChildren<Push>())
                p.Initialize();
        }

        void TurnOffRagdoll()
        {
            foreach (var rb in rigidBodies)
                rb.isKinematic = true;
        }

        public void TurnOnRagdoll()
        {
            foreach (var rb in rigidBodies)
                rb.isKinematic = false;
        }
    }
}
