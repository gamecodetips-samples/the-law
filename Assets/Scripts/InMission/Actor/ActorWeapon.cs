﻿using System.Collections;
using System.Linq;
using Assets.Scripts.InMission.Weapons;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class ActorWeapon : MonoBehaviour
    {
        private Weapon weapon;
        private ActorAnimator animator;

        public void Initialize(ActorAnimator animator, Transform viewPoint, Transform weaponGrab, Weapon weaponPrefab)
        {
            Instantiate(weaponPrefab, weaponGrab);
            this.animator = animator;
            SetCurrentWeapon(viewPoint);
        }

        public void Fire()
        {
            if (weapon && weapon.IsIdle)
            {
                if (weapon.IsEmpty)
                    Reload();
                else
                {
                    animator.Fire();
                    weapon.Fire();
                }
            }
        }

        void SetCurrentWeapon(Transform viewPoint)
        {
            weapon = GetComponentInChildren<HitscanWeapon>();
            weapon.Initialize(viewPoint, new ModifierSet());
            animator.SetWeaponHands(weapon.CurrentStats.WeaponHandCount);
        }

        public void Reload() => weapon.Reload();
    }
}
