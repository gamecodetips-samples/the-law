﻿using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class ActorAnimator : MonoBehaviour
    {
        private Animator animator;

        public void Initialize(Animator animator) => this.animator = animator;

        public void SetMoveSpeed(float moveSpeed) => animator.SetFloat("MoveSpeed", moveSpeed);

        public void Crouch() => animator.SetBool("IsCrouching", true);

        public void Stand() => animator.SetBool("IsCrouching", false);

        public void Stun() => animator.SetBool("IsStunned", true);

        public void RemoveStun() => animator.SetBool("IsStunned", false);

        public void Aim() => animator.SetBool("IsAiming", true);

        public void RestAim() => animator.SetBool("IsAiming", false);

        public void Fire() => animator.SetTrigger("Fire");

        public void StopMoving() => SetMoveSpeed(0f);

        public void GetHit() => animator.SetTrigger("GetHit");

        public void BindHands() => animator.SetBool("IsBound", true);
        public void UnbindHands() => animator.SetBool("IsBound", false);

        public void SetWeaponHands(int count) => animator.SetInteger("WeaponHands", count);
    }
}
