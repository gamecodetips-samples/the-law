﻿using System.Collections.Generic;
using System.Linq;
using LevelGenerator.Scripts.Helpers;
using UnityEngine;

namespace Assets.Scripts.InMission.Actor
{
    public class ColorRandomizer : MonoBehaviour
    {
        public Material[] Skins;
        public SkinnedMeshRenderer Mesh;

        void Start()
        {
            var newMaterials = RandomizeColors(Mesh.materials);
            Mesh.materials = newMaterials.ToArray();
        }

        private IEnumerable<Material> RandomizeColors(Material[] baseMaterials)
        {
            foreach (var mat in baseMaterials)
            {
                mat.SetColor("_BaseColor", Random.ColorHSV());
            }

            baseMaterials[3] = Skins.PickOne();

            return baseMaterials;
        }
    }
}
