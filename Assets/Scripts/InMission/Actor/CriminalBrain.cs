﻿using System;
using System.Collections;
using Assets.Scripts.InMission.Navigation;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.InMission.Actor
{
    public class CriminalBrain : MonoBehaviour
    {
        private Navigator navigator;
        private ActorAiming aiming;
        CoverSpotProvider coverProvider;
        Humanoid target;
        ActorAnimator animator;
        private CoverSpot currentCover;

        bool CoverCompromised => !currentCover.IsCoveredFrom(target.CenterOfMass);
        private bool isStunned;

        public void Initialize(Navigator navigator, CoverSpotProvider coverProvider, Humanoid target, ActorAnimator animator, ActorAiming aiming)
        {
            this.navigator = navigator;
            this.coverProvider = coverProvider;
            this.target = target;
            this.animator = animator;
            this.aiming = aiming;

            MoveToCover();
        }
        void MoveToCover()
        {
            animator.Stand();
            currentCover = coverProvider.GetClosestCoveredSpotFrom(target.CenterOfMass, transform.position);
            navigator.MoveTo(currentCover.Position, TakeCover);
        }

        void TakeCover()
        {
            aiming.StopAiming();
            animator.Crouch();

            if (CoverCompromised)
                MoveToCover();
            else
                StartCoroutine(WaitAndDo(AttackOrReposition));
        }

        void AttackOrReposition()
        {
            if (CoverCompromised)
                MoveToCover();
            else
                AttackTarget();
        }

        public void Stun(float stunDuration)
        {
            if (!isStunned)
            {
                StopAllCoroutines();
                navigator.Stop();
                animator.Stun();
                animator.Stand();
                aiming.StopAiming();
                StartCoroutine(WaitAndDo(RemoveStun));
            }
        }

        void RemoveStun()
        {
            animator.RemoveStun();
            MoveToCover();
        }

        private void AttackTarget()
        {
            animator.Stand();
            aiming.StartAiming();
            StartCoroutine(WaitAndDo(TakeCover));
        }

        IEnumerator WaitAndDo(Action onWaited)
        {
            yield return new WaitForSeconds(Random.Range(2, 6));
            onWaited();
        }

        public void Shutdown()
        {
            aiming.StopAiming();
            StopAllCoroutines();
        }
    }
}
