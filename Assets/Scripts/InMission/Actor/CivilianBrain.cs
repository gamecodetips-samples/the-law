﻿using System;
using System.Collections;
using Assets.Scripts.InMission.Navigation;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.InMission.Actor
{
    public class CivilianBrain : MonoBehaviour
    {
        private Navigator navigator;
        private NavPointProvider navigationProvider;

        public void Initialize(Navigator navigator, NavPointProvider navigationProvider)
        {
            this.navigator = navigator;
            this.navigationProvider = navigationProvider;

            Move();
        }
        void Move() => navigator.MoveTo(navigationProvider.GetRandomNavPoint(), WaitAndMove);

        void WaitAndMove()
        {
            StartCoroutine(WaitAndDo(Move));
        }

        private IEnumerator WaitAndDo(Action onWaited)
        {
            yield return new WaitForSeconds(Random.Range(2, 6));
            onWaited();
        }

        public void Shutdown() => StopAllCoroutines();
    }
}
